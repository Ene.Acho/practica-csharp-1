﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net.Http;

namespace Practica_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Ejercicio3();
        }

        public static void Ejercicio1()
        {
            int aprob = 0;
            int sobre = 0;
            double nota;

            for (int i = 0; i < 10; i++)
            {
                nota = double.Parse(Console.ReadLine());
                if (nota>=9)
                {
                    sobre++;
                }
                else if (nota >=5)
                {
                    aprob++;
                    aprob = aprob + sobre;
                }

            }
            Console.WriteLine("Sobresalientes: " + sobre);
            Console.WriteLine("Aprobados fueron " + aprob);
        }

        public static void Ejercicio2()
        {
            string fecha;
            Console.WriteLine("Inserte fecha fecha");
            Console.WriteLine("Escribir en formato dd/mm/aa");
            fecha = Console.ReadLine();
            DateTime recivido = DateTime.Parse(fecha);
            Console.WriteLine("la fecha para pagar es: ");
            DateTime pagar = recivido.AddMonths(3);
            Console.WriteLine(pagar.Day + "/" + pagar.Month + "/" + pagar.Year);
            
        }

        public static void Ejercicio3()
        {
            int[] numeros = new int[10];
            for (int i = 0; i < numeros.Length; i++)
            {
                numeros[i] = Int32.Parse(Console.ReadLine());
            }
            Console.WriteLine("");
            Console.WriteLine("Los numeros son");
            Console.WriteLine("");
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.WriteLine(numeros[i] + " su posicion " + (i+1));
            }
            Console.WriteLine("");
            Console.WriteLine("Y el orden al reves es: ");
            Console.WriteLine("");
            int e = 10;
            while (e!=0)
            {
               
                for (int i = 9; i >= 0; i--)
                {
                    Console.WriteLine(numeros[i] + " posicion " + (i+1));
                    e--;
                }
            }
        }
        public static void Ejercicio4()
        {
            string[] Empresa = new string[10];
            int[] ingresos = new int[10];
            int[] gastos = new int[10];
            int[] Diferencia = new int[10];
            double menor = 0;
            int loc = 0;

            for (int i = 0; i < Empresa.Length; i++)
            {
                Empresa[i] = "Empresa " + (i + 1);
                Console.WriteLine(Empresa[i]);
                Console.WriteLine("Inserte beneficios del mes");
                ingresos[i] = int.Parse(Console.ReadLine());
                Console.WriteLine("Inserte gastos del mes");
                gastos[i] = int.Parse(Console.ReadLine());
                Diferencia[i] = ingresos[i] - gastos[i];
                if (Diferencia[i] < menor)
                {
                    menor = Diferencia[i];
                    loc = i + 1;
                }
                Console.WriteLine("");
            }
            Console.WriteLine("Empresa " + loc);
            Console.WriteLine("Ha tenido resultado beneficio-gastos, con una perdida de");
            Console.WriteLine(menor);

        }
        public static void Ejercicio5()
        {
            int[] valores = new int[10];
            int num1 = 0;
            int num2 = 0;
            for (int i = 0; i < valores.Length; i++)
            {
               valores[i]=Int32.Parse(Console.ReadLine());
                if (valores[i]>num1)   
                {
                    num1 = valores[i];   
                }
                if ((valores[i] > num2) && (valores[i] < num1))
                {
                    num2 = valores[i];
                }
            }
            Console.WriteLine("");
            Console.WriteLine("El primer numero mayor es");
            Console.WriteLine(num1);           
            Console.WriteLine("");
            Console.WriteLine("El 2do numero mayor es");
            Console.WriteLine(num2);

        }       
    }
}
